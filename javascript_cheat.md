# Javascript cheat sheet

## Strings

Reverse a str using chaining

```Javascript
let rev = [...str].reverse().join("");
```


## Arrow functions

Get the factoral of a number

```Javascript
const FirstFactorial = num => !num ? 1 : num * FirstFactorial(--num);
```

If else shortened to ternary operator: 

```Javascript
const twentyfourhour = (ampmcheck) => {
return ampmcheck === "pm" ? 12 : 0;
}
```
Then arrow function shortened by removing brackets and return.

```JavaScript
const twentyfourhour = (ampmcheck) => ampmcheck === "pm" ? 12 : 0;
```

## REGEX

* [regex101](https://regex101.com/r/a8cfBe/3)
* [String match](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/match)
* [Regex test](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test)

1. The username is between 4 and 25 characters.
2. It must start with a letter.
3. It can only contain letters, numbers, and the underscore character.
4. It cannot end with an underscore character.

```Javascript
function SearchingChallenge(str) {
  const regex = /^[(a-zA-Z)]\w{2,23}[(a-zA-Z0-9)]{1}$/gm;
  // The match() method retrieves the result of matching a string against a regular expression.
  const checkUsername = str.match(regex);

  // test will determine if username string
  // Meets the REGEX standard required
  // boolean value returned 

  return regex.test(str);
}

// keep this function call here
//console.log(SearchingChallenge(readline()));
const satUsername = SearchingChallenge("u__hello_world123");
console.log(satUsername);
```

### Regex longest word in string

```Javascript
function LongestWord(sen) {
// match any non-whitespace characters for filtering out individual words in match function below
// Use w for 
const regex = /\w+/gm;
// Place words in Array
const actualWord = sen
.match(regex)
// Place word lengths in Array of equal elements
const word = sen
.match(regex)
.map(e => e.length);
// Determine longest word
let long = Math.max(...word);
// Determine indexOf word with longest length
let i = word.indexOf(long);
// Return word with longest length
return actualWord[i];

}

console.log(LongestWord("fun&!! time zzzzzzzzzzzzzzzzzzzzzzzzz"));
```

## Regex matching characters in Strings

```JavaScript
function StringScramble(str1,str2){

let re = new RegExp("[" + str2 + "]","g");
const found = str1.match(re).length;

if(found >= str1.length || found >= str2.length){
    return true;
  }
  else {
    return false;
  }

}


console.log(StringScramble("captain","cap"))

```
## Regex Palindrome

Regex used to filter out not alphabet characters in this chaining example.  

```JavaScript
function PalindromeTwo(str){

  let re = /[a-zA-Z]+/g;

  let clean = (str
    .match(re) || [])
    .join('')
    .toLowerCase();

  let imbackwards = [...clean]
    .reverse()
    .join('')
    .toString();

  let mtch = clean.localeCompare(imbackwards);

  let ret = (mtch === 0) ? true : false;

  return ret;

}


console.log(PalindromeTwo("Noel - sees Leon"))
console.log(PalindromeTwo("A war at Tarawa!"))
console.log(PalindromeTwo("funky monkey"))
```

## Regex Lookups with Digits

```Javascript
function CountingMinutes(str){
const startt = /^[\d]*[:]*\d*/g;
const endt = /(?<=-)\d*[:]\d*/g;
const hour = /^[\d]*/g;
const min = /(?<=[:])\d*/g;

let start = str
  .match(startt)
  .toString();

let end = str
  .match(endt)
  .toString();


let starth = start
  .match(hour)
  .toString();

let startm = start
  .match(min)
  .toString();

let endh = end
  .match(hour)
  .toString();

let endm = end
  .match(min)
  .toString();

let starthn = Number(starth);
let startmn = Number(startm);
let endhn = Number(endh);
let endmn = Number(endm);


console.log("starthn",starthn);
console.log("startmn",startmn);
console.log("endhn",endhn);
console.log("endmn",endmn);

}

console.log(CountingMinutes("12:30pm-12:23am"));

```

# Chaining examples

Filter String ensuring no spaces with filter, map each characters ascii number, move ascii number by 8, then get char of that number, finally join String back together:

```JavaScript

let strnum = [...str]
  .filter(e => e != ' ')
  .map((e) => (e = String.fromCharCode((e.charCodeAt(e) + 8))))
  .join('')

```

## Unicode Characters
* [charCodeAt](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/charCodeAt)
Get unicode number and print String or character from Unicode.
* [fromCharCode](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/fromCharCode)

```Javascript
str = "foo"

let thisChar = str.charCodeAt(0);
let nextChar = thisChar + 1;

console.log(thisChar);

console.log(String.fromCharCode(thisChar));
console.log(String.fromCharCode(nextChar));
```

# Sorting

Example below sorting the number values of the word `foowordz` in Unicode. 

```Javascript
  str = foowordz

  let alphabet = [...str]
  .map((e) => (e = e.charCodeAt(e)))
  .sort((a,b) => a - b)

```
## Sorting String array by length
* [sort array based on length](https://stackoverflow.com/questions/10630766/how-to-sort-an-array-based-on-the-length-of-each-element)

Decending Order:

```Javascript
  strArr.sort((a, b) => b.length - a.length);
  return strArr[2];
```
Or Accending order:

```Javascript
  strArr.sort((a, b) => a.length - b.length);
  return strArr[2];
```

# Fibonacci sequence check

* [Binet Formula](https://artofproblemsolving.com/wiki/index.php?title=Binet%27s_Formula)
* [Test Fibonacci Number](https://www.baeldung.com/cs/test-fibonacci-number)

```Javascript
// Binet Formula
// If either 5x^2 + 4 or 5x^2 - 4 is a perfect square
// Number is part of the Fibonacci squence

function binetFormulaPos(num){
return (5 * num * num) + 4;
}

function binetFormulaNeg(num){
  return (5 * num * num) - 4;
}

function perfectSquare(num){
let perfect = Math.floor(Math.sqrt(num));

if((perfect * perfect) === num){
return true;
}
else {
return false;
}
}

function FibonacciChecker(num){
// 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144.

let pos = binetFormulaPos(num);
let neg = binetFormulaNeg(num);

if(perfectSquare(pos) || perfectSquare(neg)){
return true;
}
else{
return false;
}


}

let num = 54;

console.log(FibonacciChecker(num));
```
# Objects

## Basic keyed Object create from array

```Javascript
strArr = ["what", "what", "chicken", "but"];

let obj = [];

strArr.forEach((e, i) => {
    obj[`key${i}`] = e
  })

console.log(obj);
```

## Map object with string and its length

```Javascript
strArr = ["what", "what", "chicken", "but"];
```
Use Map to set each string and its length.

```Javascript
const mobj = new Map();

  strArr.forEach(e => {
  mobj.set(e, e.length);
  });
```
Now print each stored key and value in new object.

```JavaScript

for(const [key,value] of mobj){
    console.log(`${key} has the length of ${value}`);
  }

}

```
This should print something like:

```Javascript
what has the length of 4
chicken has the length of 7
but has the length of 3
```


## Using Reduce to create an Object

```Javascript
arr = [1,1,2,3,6,6,7,-3];
```

```Javascript
const countedNums = arr.reduce((allNums, num) => {
  const currCount = allNums[num] ?? 0;
  return {
    ...allNums,
    [num]: currCount + 1,
  };
}, {});

```

## Created Object
The above reduce function should create the following object, note the dual 6 and 1 have the value 2:

```Javascript
{ '1': 2, '2': 1, '3': 1, '6': 2, '7': 1, '-3': 1 }
```

Then iterate through created Object and determine duplicates as discovered when creating the Object, in this example array 2 duplicates are found:

```Javascript
for(const [key, value] of Object.entries(countedNums)){
      //console.log(`${key}: ${value}`);
    if(value > 1){
      count += value - 1;
    }

  }
```

# DOM

## Removing items

A function to remove the first child item `li` from a `ul`.

```Javascript
function removeFirstItem() {
  const ul = document.querySelector('ul');
  const li = document.querySelector('li:first-child');

  ul.removeChild(li);

}

```
create function to remove a user provided `child` number `li` from a `ul`. 

```Javascript

function removeItem(itemNumber){

  const ul = document.querySelector('ul');
  const li = document.querySelector(`li:nth-child(${itemNumber})`);

  ul.removeChild(li);

}

removeItem(1);

```

Or as an arrow function

```Javascript
const removeI = (itemNumber) =>
  document.querySelectorAll('li')[itemNumber - 1].remove();

removeI(2);
```

