# Corepack Install

Remove yarn if already installed:

```bash
npm uninstall -g yarn pnpm
```
Install corepack which includes `yarn`

```bash
npm install -g corepack
```

# Yarn project setup cheats

```bash
mkdir MyJavascriptProject
```

```bash
cd MyJavascriptProject
```

Initialize yarn project

```bash
yarn init
yarn install
```

* [editor sdk](https://yarnpkg.com/getting-started/editor-sdks)
* [neovim-native-lsp](https://yarnpkg.com/getting-started/editor-sdks#neovim-native-lsp)

Neovim lisp support. Run the following command, which will generate a new directory called .yarn/sdks and create a .vim/coc-settings.json file:

```bash
yarn dlx @yarnpkg/sdks base
```
VIM support

```bash
yarn dlx @yarnpkg/sdks vim
```
VS Code support

```bash
yarn dlx @yarnpkg/sdks vscode
```
## Setup git repository for project

### Pre-check
If not done previously install git and setup proper global settings.

```bash
git config --global user.email "youmail@protonmail.com"
git config --global user.name "youname"
git config --global core.editor "vim"
```
### Make sure you are in the root of your project

```bash
cd MyJavascriptProject
```
Normally with a empty project you would need to run `git init` at this point. This has alrteady been handled by yarn with `yarn init`. You should already see the `.git` directory precreated within your yarn project. 

Add all files to project.

```bash
git add .
```
Commit additions to project

```bash
git commit
```
In this example push your new project to your gitlab account `youname`. Assuming you have working ssh keys on gitlab this will create a new project as detailed by your project name.  

```bash
git push --set-upstream git@gitlab.com:youname/$(git rev-parse --show-toplevel | xargs basename).git $(git rev-parse --abbrev-ref HEAD)
```

# Linters

* [ESLINT](https://eslint.org/docs/latest/user-guide/getting-started)

Add eslint libraries to your project:

```bash
yarn add eslint --dev
```
Create eslint config

```bash
yarn create @eslint/config
```
You should now have something similar to the following:

```bash
Successfully created .eslintrc.js file
```

```javascript
module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "standard"
    ],
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "rules": {
    }
}
```

Begin linting your javascript:

* [command line interface](https://eslint.org/docs/latest/user-guide/command-line-interface)

```bash
yarn run eslint yourfile.js
```
Or with fixing as many issues as possible while linting:

```bash
yarn eslint --fix yourfile.js
```

# Use NPM to search and install files

```bash
npm search coc
```
# Typescript install

```bash
yarn add --dev typescript
```
* [https://www.typescriptlang.org/docs/handbook/2/basic-types.html](https://www.typescriptlang.org/docs/handbook/2/basic-types.html)

Run `tsc` via yarn with:

```bash
yarn tsc
```

# Javascript Unit Testing
## Jest
Yarn appears to have some issues with Jest. Until I can figure out why this works:

```bash
sudo npm install jest --global
```
Add jest as a script to your projects `package.json` file:

```json
{
    "name": "Code-Platoon-Repeat-Chars",
    "packageManager": "yarn@3.2.1",
    "scripts": {
      "test": "jest"
    },
    "devDependencies": {
      "eslint": "^8.0.1",
      "eslint-config-standard": "latest",
      "eslint-plugin-import": "^2.25.2",
      "eslint-plugin-n": "^15.0.0",
      "eslint-plugin-promise": "^6.0.0",
      "jest": "^28.1.3",
      "jest-cli": "^28.1.3"
    }
  }
```
Make a `test.filefoo.js` for the `filefoo.js` you are testing. Then make sure the `function` you intend to test is exported. In this example the function is `function repeatedChars(charArray, numArray)`:

```javascript
module.exports = repeatedChars;
```
Now the `test.filefoo.js` can be reached by the test file via [jest](https://jestjs.io/docs/getting-started)

```javascript
const repeatedChars = require('./filefoo');
  
  test('Converts two arrays to single String', () => {
          expect(repeatedChars(['a','b','c'], [1,2,3])).toContain('abbccc');
  });
```
Run the Unit test with simply jest:

```bash
jest
```
Or with yarn as a script:

```bash
yarn test
```
# node errors
The following errors or similar will require some edits to the javascript file:

```bash
(node:119343) Warning: To load an ES module, set "type": "module" in the package.json or use the .mjs extension.
(Use `node --trace-warnings ...` to show where the warning was created)
/home/trentonknight/exercism/javascript/high-score-board/high-score-board.js:9
export function createScoreBoard() {
^^^^^^

SyntaxError: Unexpected token 'export'
```
To resolve this issue all exporting functions such as:

```javascript
export function addPlayer(scoreBoard, player, score) {
    throw new Error('Please implement the addPlayer function');
  }
```
Will need to have `export` removed from before the function:

```javascript
function addPlayer(scoreBoard, player, score) {
    throw new Error('Please implement the addPlayer function');
  }
  ```
Then to export the function add the following to the bottom of the `js` file:

```javascript
module.exports = {addPlayer};
```
Add all functions in the script similarly after removing `export` from the begining of each function:
```javascript
module.exports = {createScoreBoard,addPlayer,removePlayer,updateScore,applyMondayBonus,normalizeScore};
```











